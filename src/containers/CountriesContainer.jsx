import React, { useEffect, useState } from "react";
import * as RESTCountriesAPI from "../services/countries";
import FilterBarRegion from "../components/FilterBarRegion";
import FilterBarSubRegion from "../components/FilterBarSubRegion";
import SortBy from "../components/SortBy";
import Cards from "../components/Cards";
import LandLocked from "../components/LandLocked";


const CountriesContainer = (props) => {
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [searchText, setSearchText] = useState("");
  const [filterRegion, setFilterRegion] = useState("");
  const [filterSubRegion, setFilterSubRegion] = useState("");
  const [sortingCriteria, setSortingCriteria] = useState("");
  const [landlocked, setLandlocked] = useState(false)

  useEffect(() => {
    RESTCountriesAPI.getCountries()
      .then((countries) => {
        setLoading(false);
        setCountries(countries);
        setError(null);
      })
      .catch((err) => setError(err.message));
  }, []);

  if (error) {
    return <h1>{error}</h1>;
  }

  if (loading) {
    return <h1>Loading...</h1>;
  }

  if (countries.length < 1) {
    return <h1>No countries found!</h1>;
  }

  const handleSearch = (event) => {
    setSearchText(event.target.value.toLowerCase());
  };

  const handleFilter = (event) => {
    setFilterSubRegion("")
    setFilterRegion(event.target.value);
  };

  const handleFilterSub = (event) => {
    setFilterSubRegion(event.target.value);
  };

  const handleCheckBox = (event) => {
    setLandlocked(!landlocked)
  }

  const handleSort = (event) => {
    setSortingCriteria(event.target.value);
  };

  const subRegionArray = Object.values(countries).reduce(
    (acc, country) => {
      if (!acc.All.includes(country.subregion)) {
        acc.All.push(country.subregion);
      }
      if (acc[country.region]) {
        if (!acc[country.region].includes(country.subregion)) {
          acc[country.region].push(country.subregion);
        }
      } else {
        acc[country.region] = [country.subregion];
      }
      return acc;
    },
    { All: [] }
  );

  const filteredCountries = Object.values(countries)
    .filter((country) => {
      return (
        (searchText === "" ||
          country.name.common.toLowerCase().includes(searchText.trim())) &&
        (filterRegion === "" || country.region === filterRegion) &&
        (filterSubRegion === "" || country.subregion === filterSubRegion) &&
        (landlocked === false || country.landlocked === true)
      );
    })
    .sort((a, b) => {
      if (sortingCriteria === "Population") {
        return a.population > b.population ? -1 : 1;
      } else if (sortingCriteria === "Area") {
        return a.area > b.area ? -1 : 1;
      } else {
        return a.name.common > b.name.common ? 1 : -1;
      }
    });

  return (
    <main>
      <section id="custom">
        <input
          className={`${props.theme}-element`}
          type="text"
          name="searchBar"
          id="searchBar"
          placeholder="Search a country..."
          onChange={handleSearch}
          value={searchText}
        />
        <FilterBarRegion
          theme={props.theme}
          countries={countries}
          handleFilter={handleFilter}
        />
        <FilterBarSubRegion
          theme={props.theme}
          subRegionArray={subRegionArray}
          region={filterRegion}
          handleFilterSub={handleFilterSub}
        />
        <SortBy theme={props.theme} handleSort={handleSort} />
        <LandLocked theme={props.theme} handleCheckBox={handleCheckBox}/>
      </section>
      <Cards theme={props.theme} filteredCountries={filteredCountries} />
    </main>
  );
};

export default CountriesContainer;
