import React, { Component, useState } from 'react';
import Header from './components/Header';
import CountriesContainer from './containers/CountriesContainer';

function App() {

    const [theme, setTheme] = useState("light")

    const themeChange = () => {
        if(theme==="light"){
            setTheme("dark")
        } else {
            setTheme("light")
        }
    }

    return ( 
        <main className={`${theme}-background`}>
        <Header themeChange={themeChange} theme={theme}/>
        <CountriesContainer theme={theme}/>
        </main>
     );
}

export default App;