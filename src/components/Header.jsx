import React, { Component } from "react";

function Header(props) {
  return (
    <div id="header" className={`${props.theme}-element`}>
      <h3>Where in the world?</h3>
      <div id="theme-div">
        <img id="theme-icon" src="src/assets/dark-mode.png" alt="" />
        <p id ="theme-p" onClick={() => props.themeChange()}>
          {props.theme === "light" ? "Dark Mode" : "Light Mode"}
        </p>
      </div>
    </div>
  );
}

export default Header;
