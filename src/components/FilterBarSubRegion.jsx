import React, { Component, useState } from "react";

function FilterBarSubRegion(props) {
  const array =
    props.region === ""
      ? props.subRegionArray.All
      : props.subRegionArray[props.region];

  return (
    <button className={`${props.theme}-element filter-bar`}>
      <select className={`${props.theme}-element`} id="filter-bar-select">
        <option
          key="All"
          className={`${props.theme}-element`}
          value=""
          onClick={props.handleFilterSub}
        >
          Filter: All Sub-Regions
        </option>
        {array.map((subregion) => {
          return (
            <option
              key={subregion}
              className={`${props.theme}-element`}
              value={subregion}
              onClick={props.handleFilterSub}
            >
              {subregion}
            </option>
          );
        })}
      </select>
    </button>
  );
}

export default FilterBarSubRegion;
