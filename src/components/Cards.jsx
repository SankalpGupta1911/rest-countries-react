import React, { Component } from "react";
import Card from "./Card";

function Cards(props) {
  return (
    <div id="cards">
      {props.filteredCountries.length > 0 ? (
        props.filteredCountries.map((country) => {
          return <Card key={country.cca3} country={country} theme={props.theme}/>;
        })
      ) : (
        <h1>No Countries Found!</h1>
      )}
    </div>
  );
}

export default Cards;
