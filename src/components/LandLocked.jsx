import React, { Component } from "react";

function LandLocked(props) {
  return (
    <div className={`${props.theme}-element filter-bar`} id="landlockedContainer">
      Landlocked:
      <input type="checkbox" name="landlocked" id="landlocked" onChange={props.handleCheckBox} />
    </div>
  );
}

export default LandLocked;
