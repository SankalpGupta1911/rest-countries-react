import React, { Component } from "react";

function Card(props) {
  return (
    <div className={`${props.theme}-element card`}>
      <div className="flagSection">
        <img src={props.country.flags.svg} alt="" />
      </div>
      <div id="detailSection">
        <p id="cardName">{props.country.name.common}</p>
        <div id="details">
            <p>Population:{props.country.population}</p>
            <p>Region:{props.country.region}</p>
            <p>Capital:{props.country.capital}</p>
        </div>
      </div>
    </div>
  );
}

export default Card;
