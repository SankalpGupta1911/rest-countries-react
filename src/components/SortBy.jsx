import React, { Component } from "react";

function SortBy(props) {
  const list = ["Population", "Area"];

  return (
    <button className={`${props.theme}-element filter-bar`}>
      <select className={`${props.theme}-element`} id="filter-bar-select">
      <option
          className={`${props.theme}-element`}
          value=""
          onClick={props.handleSort}
        >
          Sort by:
        </option>
        {list.map((criteria) => {
          return (
            <option
              key={criteria}
              className={`${props.theme}-element`}
              value={criteria}
              onClick={props.handleSort}
            >
              {criteria}
            </option>
          );
        })}
      </select>
    </button>
  );
}
export default SortBy;
