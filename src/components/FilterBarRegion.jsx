import React, { Component } from "react";

function FilterBarRegion(props) {
  const list = ["Africa", "Americas", "Asia", "Europe", "Oceania"];

  return (
    <button className={`${props.theme}-element filter-bar`}>
      <select className={`${props.theme}-element`} id="filter-bar-select">
        <option
          className={`${props.theme}-element`}
          value=""
          onClick={props.handleFilter}
        >
          Filter: All Regions
        </option>
        {list.map((region) => {
          return (
            <option
              key={region}
              className={`${props.theme}-element`}
              value={region}
              onClick={props.handleFilter}
            >
              {region}
            </option>
          );
        })}
      </select>
    </button>
  );
}

export default FilterBarRegion;
